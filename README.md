# README #

This is the Force Balance script written by Mark Bustin at Mavi Innovations for Brownlie Management Ltd

### What is this repository for? ###

* The forcebalance script provides a simple GUI to interface with the LabJack U6 and perform the desired testing of a load cell.

### How do I get set up? ###

* Install the following packages:
* Anaconda 4.4.0 from https://www.anaconda.com/download/  (Contains: Python 3.6.1, Numpy 1.12.1)
* Labjack windows installers from https://labjack.com/support/software/installers
* LabjackPython from https://github.com/labjack/LabJackPython

### How do I run this? ###

* After installing the packages above, double click on forcebalance.py
* Or if you want, you can run it from a command line (like PowerShell). Navigate to the forcebalance directory and type 'python forcebalance.py'
* The app will produce a GUI with two buttons: 'Get 0' and 'Run Test'
* 'Get 0' will produce a zero value that will then be used in the 'Run Test' operation to calculate the drag forces (it removes the forces experienced by the load cell at its resting state)
* The zero acquired by 'Get 0' is placed in the entry box labelled 'Data Conversion: Zero (V)'. Whatever number is in this entry box is what is used by 'Run test'. This is so that if the application is accidentally closed, the user can input their own zero value without having to reset the test procedure to re-run the 'Get 0' stage of the test.
* Data is dumped to the data folder of the forcebalance directory. 'TESTNAME_stat_results.csv' contains the means and std devs, this file is appended to every time a test is run. NOTE - if this file is not present then it is automatically made with header inserted. 'TESTNAME_test_results_TIMENOW.csv' contains the sampled data from a test, a new file is made each time a test is run.

### Who do I talk to? ###

* Mark Bustin: mark@bustin.io
