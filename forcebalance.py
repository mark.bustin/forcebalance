#!/usr/bin/env python3
"""
This is the starting point for the Force Balance Script.

Start up the tkinter object and populate the gui, then run the main loop.
"""

import tkinter as tk
import forcebalance.gui as gui
import forcebalance.menu_items as menu_items
#**********************************************************************
#********************       Begin main       **************************
#**********************************************************************

if __name__ == "__main__":
    root = tk.Tk()
    menu = gui.Window(root,
                      menu_items.inputs,
                      menu_items.outputs)
    root.mainloop()
