"""
This is the driver for the script.
It is what is called from the GUI to run testing.
"""

import sys
import os.path
import csv
import traceback
import u6
import math
from datetime import datetime
import numpy as np


class Engine(object):

    def __init__(self):
        pass

    def get_0(self, test_parameters):
        """
        Run a test for the duration and return the raw mean and std dev of
        the voltages measured
        """

        ###Some data Containers
        duration = int(float(test_parameters['Test Duration (s)']))
        SCAN_FREQUENCY = int(float(test_parameters['Frequency (Hz)']))
        MAX_REQUESTS = int( math.ceil((SCAN_FREQUENCY * duration) / (48 * 25)) ) #number of samples per packet (25), num packets per request (48)
        num_namples = MAX_REQUESTS * 48 * 25

        ###Gain conversion
        GAIN = self.gain_calc(int(test_parameters['Gain']))

        #Run Test
        self.zero = np.zeros(num_namples, dtype=float)
        self.zero = self.start_stream(duration, SCAN_FREQUENCY, MAX_REQUESTS, self.zero, GAIN)
        self.Vmean_zero = np.mean(self.zero)
        self.Vstd_dev_zero = np.std(self.zero)

        #Dump Data
        self.data_dump(self.zero, self.Vmean_zero, self.Vstd_dev_zero,
                       test_parameters['Test Name'], test_parameters['Filepath'], 'zero')

        return self.zero, self.Vmean_zero, self.Vstd_dev_zero

    def run_test(self, test_parameters):
        """
        Run a test for the duration and return the mean and std dev of
        the forces measured. The voltages are converted using the given slope
        and offset.
        """

        ###Some data Containers
        zero = float(test_parameters['Data Conversion: Zero (V)'])
        duration = int(float(test_parameters['Test Duration (s)']))
        SCAN_FREQUENCY = int(float(test_parameters['Frequency (Hz)']))
        MAX_REQUESTS = int( math.ceil((SCAN_FREQUENCY * duration) / (48 * 25)) ) #number of samples per packet (25), num packets per request (48)
        num_namples = MAX_REQUESTS * 48 * 25
        slope = float(test_parameters['Data Conversion: Slope'])
        offset = float(test_parameters['Data Conversion: Offset'])

        ###Calc GAIN
        GAIN = self.gain_calc(int(test_parameters['Gain']))

        #Run Test
        self.test_results = np.zeros(num_namples, dtype=float)
        self.test_results = self.start_stream(duration, SCAN_FREQUENCY, MAX_REQUESTS, self.test_results, GAIN)
        self.adjusted_results = self.test_results - zero

        #Calc Results
        self.converted_results = slope * self.adjusted_results + offset
        self.Fmean = np.mean(self.converted_results)
        self.Fstd_dev = np.std(self.converted_results)

        #Dump Data
        self.data_dump(self.converted_results, self.Fmean, self.Fstd_dev,
                       test_parameters['Test Name'], test_parameters['Filepath'], 'test_results')

        return self.converted_results, self.Fmean, self.Fstd_dev

    def start_stream(self, duration, freq, num_requests, samples_array, GAIN):

        ###U6 initialization
        daq = None
        daq = u6.U6()
        if daq is None:
            print("""Failure to communicate with U6""")
            sys.exit(0)
        daq.getCalibrationData()
        daq.streamConfig(NumChannels=1,
                         ChannelNumbers=[0],
                         ChannelOptions=[GAIN],
                         SettlingFactor=1,
                         ResolutionIndex=6,
                         ScanFrequency=2000)

        #Test Parameters
        missed = 0
        dataCount = 0
        packetCount = 0
        i = 0

        try:
            print("Start stream")
            daq.streamStart()
            start = datetime.now()
            print("Start time is %s" % start)

            for values in daq.streamData():
                if values is not None:

                    if dataCount >= num_requests:
                        break

                    if values['errors'] != 0:
                        print('errors counted: %s' % (values['errors']))

                    if values["numPackets"] != daq.packetsPerRequest:
                        print("----- UNDERFLOW : %s ; %s" % (values["numPackets"], datetime.now()))

                    if values["missed"] != 0:
                        missed += values['missed']
                        print("+++ Missed %s" % values["missed"])

                    # Comment out these prints and do something with values
                    print("Average of %s AIN0, %s, len = %s" %
                          (len(values["AIN0"]), sum(values["AIN0"]) / len(values["AIN0"]), len(values['AIN0'])))

                    for val in values["AIN0"]:
                        samples_array[i] = val
                        i += 1


                    dataCount += 1
                    packetCount += values['numPackets']
                else:
                    # Got no data back from our read.
                    # This only happens if your stream isn't faster than the USB read
                    # timeout, ~1 sec.
                    print("No data ; %s" % datetime.now())
        except:
            print("".join(i for i in traceback.format_exc()))

        finally:
            stop = datetime.now()
            daq.streamStop()
            print("Stream stopped.\n")
            daq.close()
            #
            # sampleTotal = packetCount * daq.streamSamplesPerPacket
            #
            # scanTotal = sampleTotal / 2  # sampleTotal / NumChannels
            # print("%s requests with %s packets per request with %s samples per packet = %s samples total." %
            #       (dataCount, (float(packetCount)/dataCount), daq.streamSamplesPerPacket, sampleTotal))
            # print("%s samples were lost due to errors." % missed)
            # sampleTotal -= missed
            # print("Adjusted number of samples = %s" % sampleTotal)
            #
            # runTime = (stop-start).seconds + float((stop-start).microseconds)/1000000
            # print("The experiment took %s seconds." % runTime)
            # print("Actual Scan Rate = %s Hz" % freq)
            # print("Timed Scan Rate = %s scans / %s seconds = %s Hz" %
            #       (scanTotal, runTime, float(scanTotal)/runTime))
            # print("Timed Sample Rate = %s samples / %s seconds = %s Hz" %
            #       (sampleTotal, runTime, float(sampleTotal)/runTime))

            return samples_array[1:]

    def data_dump(self, data, mean, std_dev, testname, filepath, test_type):
        """
        Dumps results to two files:
            - statistics results from both 'zeros' and 'test results' are dumped
              to the same file which is just a csv:
            - sampled data from either 'zeros' or 'test results' are stored in their
              own file files whose filename is timestamped

        Timestamps for filenames and in csv files are represented as 'H-M-S' because
        windows hates basically all other punctuation
        """
        now = datetime.now()
        time = now.strftime('%I-%M-%S')
        statsname = testname + '_' + 'stat_results.csv'
        dataname = testname + '_' + test_type + '_' + time + '.csv'

        statspath = os.path.join(filepath, statsname)
        datapath = os.path.join(filepath, dataname)
        file_exists = os.path.isfile(statspath)

        with open(statspath, 'a') as testcsv:
            writer = csv.writer(testcsv, quoting=csv.QUOTE_NONE)

            if not file_exists:
                writer.writerow(['Test Name',
                                 'Test Type',
                                 'Time (h-m-s)',
                                 'Mean',
                                 'Std Dev'])

            writer.writerow([testname, test_type, time, mean, std_dev])

        with open(datapath, 'w') as testcsv:
            writer = csv.writer(testcsv, quoting=csv.QUOTE_NONE)

            if test_type == 'zero':
                writer.writerow(['Volts (V)'])
            elif test_type == 'test_results':
                writer.writerow(['Force (g)'])

            for row in data:
                writer.writerow([row])

    def gain_calc(self, GAIN):
        '''
        Calculates the appropriate value to be written to the U6 control register.
        Both the gain and the differential input state is defined in this register.
        Gain can only b 1, 10, 100 or 1000. Any value inbetween these values is rounded down.
        '''
        GAIN = abs(GAIN)
        if GAIN < 10:
            GAIN = 0b10000000
        if 10 <= GAIN < 100:
            GAIN = 0b10010000
        if 100 <= GAIN < 1000:
            GAIN = 0b10100000
        if 1000 <= GAIN:
            GAIN = 0b10110000

        return GAIN
