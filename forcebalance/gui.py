"""
This is the gui building and populating script.

There is a single class for making a window and populating it with default menu
options.
"""

import tkinter as tk
import collections as coll
import copy as copy
import numpy as np
import matplotlib
import quantiphy as qtphy
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

import forcebalance.menu_items as menu_items
# import forcebalance.engine as engine

class Window(tk.Frame):
    """
    This class builds a window with the desired inputs, buttons, and plots.
    """

    def __init__(self, parent, *args):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.parent.configure(background='grey94')
        self.master.title('Force Balance Platform Application')
        self.grid()

        # self.test_engine = engine.Engine()

        self.row_num = 0
        self.col_num = 0

        self.populate_inputs(parent, args[0])
        self.make_buttons(parent)
        self.populate_outputs(parent, args[1])
        self.make_output_labels(parent, args[1])
        nodata = np.zeros(10)
        self.plot_results(nodata, 0, 0, 'Empty Plot', 'X', 'Y')

    def populate_inputs(self, parent, inputs_dict):
        """Take the input dict and make labels from key, entries from val"""
        self.input_entries = {}

        for key, val in inputs_dict.items():

            tk.Label(parent, text=key).grid(row=self.row_num, column= self.col_num, columnspan=2, sticky=tk.E)
            self.input_entries[key] = tk.Entry(parent)
            self.input_entries[key].delete(0, tk.END)
            self.input_entries[key].insert(0, val)
            self.input_entries[key].grid(row=self.row_num, column= self.col_num+2, columnspan=2, sticky=tk.W)
            self.row_num += 1


    def make_buttons(self, parent):
        """Take the buttons dict and make buttons from the contents"""

        self.get0button = tk.Button(parent, text='Get 0', command=self.capture_and_get0)
        self.get0button.config(background='blue')
        self.get0button.grid(row=self.row_num, column= self.col_num, columnspan=2)
        self.testbutton = tk.Button(parent, text='Run Test', command=self.capture_and_test)
        self.testbutton.config(background='green')
        self.testbutton.grid(row=self.row_num, column= self.col_num+2, columnspan=2)

        self.row_num += 2


    def populate_outputs(self, parent, outputs_dict):
        """Take the output dict and make labels from keys and vals"""
        self.output_entries = {}

        for i, (key, val) in enumerate(outputs_dict.items()):

            tk.Label(parent, text=key).grid(row=self.row_num, column= self.col_num, sticky=tk.E)
            self.output_entries[key] = tk.Entry(parent)
            self.output_entries[key].grid(row=self.row_num, column= self.col_num+1, sticky = tk.W)
            self.row_num += 1

            if i % 2 == 1:
                self.row_num -= 2
                self.col_num += 1

    def make_output_labels(self, parent, output_labels):
        self.output_labels_text = [tk.StringVar(), tk.StringVar()]
        self.output_labels_text[0].set("Captured Mean:")
        self.output_labels_text[1].set("Captured Std Dev:")

        tk.Label(parent, textvariable=self.output_labels_text[0], font=('Arial', 16)).grid(row=self.row_num, column=self.col_num+2)#, columnspan=2)
        tk.Label(parent, textvariable=self.output_labels_text[1], font=('Arial', 16)).grid(row=self.row_num, column=self.col_num+4)#, columnspan=2)

    def capture_and_get0(self):
        """
        Takes the test parameters as inputs, runs a test for the chosen duration,
        dumps data to files and the gui.

        No math is performed on the data points other than calculating the mean and
        standard deviation. The mean is used to set the zero value input entry box
        which is then used when calculating the forces of a real test.
        """
        self.red_screen()
        test_parameters = {}

        for key, entry in self.input_entries.items():
            test_parameters[key] = entry.get()

        # data, mean, std_dev = self.test_engine.get_0(test_parameters)

        self.output_entries['Zero Mean (V)'].delete(0, tk.END)
        self.output_entries['Zero Std Dev (V)'].delete(0, tk.END)
        self.input_entries['Data Conversion: Zero (V)'].delete(0, tk.END)
        self.output_entries['Zero Mean (V)'].insert(0, mean)
        self.output_entries['Zero Std Dev (V)'].insert(0, std_dev)
        self.input_entries['Data Conversion: Zero (V)'].insert(0, mean)

        title = 'Force Balance Results: Zero Baseline'
        xlabel = 'Samples'
        ylabel = 'Volts (V)'
        self.plot_results(data, mean, std_dev, title, xlabel, ylabel)

        self.grey_screen()

    def capture_and_test(self):
        self.red_screen()
        test_parameters = {}

        for key, entry in self.input_entries.items():
            test_parameters[key] = entry.get()

        # data, mean, std_dev = self.test_engine.run_test(test_parameters)

        self.output_entries['Test Mean (g)'].delete(0, tk.END)
        self.output_entries['Test Std Dev (g)'].delete(0, tk.END)
        self.output_entries['Test Mean (g)'].insert(0, mean)
        self.output_entries['Test Std Dev (g)'].insert(0, std_dev)

        title = 'Force Balance Results: Test'
        xlabel = 'Samples'
        ylabel = 'Force (g)'
        self.plot_results(data, mean, std_dev, title, xlabel, ylabel)

        self.grey_screen()

    def red_screen(self):
        self.parent.configure(background='red')
        self.parent.update_idletasks()


    def grey_screen(self):
        self.parent.configure(background='grey94')
        self.parent.update_idletasks()

    def plot_results(self, data, mean, std_dev, title, xlabel, ylabel):
        """
        Plot the data results on the graph and update the mean and std dev labels.
        """

        self.update_labels(mean, std_dev, ylabel)

        #plot stuff
        time = np.arange(len(data))
        fig = Figure()
        a = fig.add_subplot(111)
        a.plot(time,data)
        a.axhline(y=mean, color='r', linestyle='-')

        a.set_title(title)
        a.set_xlabel(xlabel, fontsize=14)
        a.set_ylabel(ylabel, fontsize=14)
        fig.tight_layout()

        #Place in tkinter window
        canvas = FigureCanvasTkAgg(fig, master=self.parent)
        canvas.get_tk_widget().grid(row=0, column=4, rowspan=self.row_num, columnspan=4,
                                    padx=5, pady=5)
        canvas.draw()

    def update_labels(self, mean, std_dev, ylabel):

        if len(ylabel) <= 1:
            ylabel = 'N/A'
        else:
            ylabel = ylabel[-2]

        mean_qty = qtphy.Quantity(mean, ylabel)
        std_dev_qty = qtphy.Quantity(std_dev, ylabel)
        mean_string = 'Captured Mean:' + str(mean_qty)
        std_dev_string = 'Captured Std Dev: ' + str(std_dev_qty)
        self.output_labels_text[0].set(mean_string)
        self.output_labels_text[1].set(std_dev_string)
