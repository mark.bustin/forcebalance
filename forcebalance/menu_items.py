"""
This module contains the default values to be presented to the user on run.

It is simply a series of ordered dicts that contain the options required.

"""

import collections as coll
import datetime as dt

inputs = coll.OrderedDict([
         ('Test Name', 'UBC_Test'),
         ('Filepath', r'C:\Users\first\Desktop\forcebalance\data'),
         ('Test Duration (s)', '10.0'),
         ('Frequency (Hz)', '2000'),
         ('Gain', '1000'),
         ('Data Conversion: Slope', '1'),
         ('Data Conversion: Zero (V)', '0')

])

outputs = coll.OrderedDict([
          ('Zero Mean (V)', ' '),
          ('Zero Std Dev (V)', ' '),
          ('Test Mean (g)', ' '),
          ('Test Std Dev (g)', ' ')
          ])
